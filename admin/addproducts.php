
<?php
include('header.php');
include('connection.php');
if((isset($_POST['submit']))){
	$product_name=$_POST['product_name'];
	$product_price=$_POST['product_price'];	
	$product_category=$_POST['product_category'];
	$img_tmp = $_FILES['product_image']['tmp_name'];
    $img = $_FILES['product_image']['name'];
    $path = 'upload/'.$img;
     $insertquery = "insert into products (product_name,product_price,product_category,product_image) values ('$product_name','$product_price','$product_category','$path')";

	move_uploaded_file($img_tmp, $path);
		$query1=mysqli_query($conn,$insertquery);
		if($query1){
			?>
			<script type="text/javascript"> 
			alert("Added!!"); location="index.php";
		</script>
				
			<?php
			
		}else{
			?>
			<script type="text/javascript"> 
			alert("Not Added"); 
				location="addproducts.php";
		</script>
			
			<?php
			
		}	

}
?>

?>


<section role="main" class="content-body">
					<header class="page-header">
						<h2>Basic Forms</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Forms</span></li>
								<li><span>Categories</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
						<div class="row">
							<div class="col-lg-12">
								<section class="panel">
									<header class="panel-heading">
										<div class="panel-actions">
											<a href="#" class="fa fa-caret-down"></a>
											<a href="#" class="fa fa-times"></a>
										</div>
						
										<h2 class="panel-title">Form Elements</h2>
									</header>
									<div class="panel-body">
										<form class="form-horizontal form-bordered" method="POST" enctype='multipart/form-data'> 
											<div class="form-group">
												<label class="col-md-3 control-label" for="inputRounded">Product Name:</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-rounded" name="product_name" id="inputRounded">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label" for="inputRounded">Product Price:</label>
												<div class="col-md-6">
													<input type="text" class="form-control input-rounded" name="product_price" id="inputRounded">
												</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label" for="inputRounded">Product Category:</label>
												<?php 
												 $sql = "select * from categories";
                            					$query = mysqli_query($conn,$sql);
                            					if (mysqli_num_rows($query) > 0) {
                            					// output data of each row
                            					 					
												?>
												<div class="col-md-6">
												<select class="form-control input-rounded" name="product_category" id="inputRounded">
													<?php
													while($row = mysqli_fetch_assoc($query)) { ?>
												<option><?php echo $row['cat_name']; ?></option>
												
											<?php }} ?>
										</select>
											</div>
											</div>
											<div class="form-group">
												<label class="col-md-3 control-label" for="inputRounded">Product Image:</label>
												<div class="col-md-6">
													<input type="file" class="form-control input-rounded" name="product_image" id="inputRounded">
												</div>
											</div>
											<div class="col-sm-4 ">
									
								
									
									<input type="submit" name="submit" class="btn btn-primary  " value="Submit">
								</div>
										</form>
									</div>
								</section>
<?php
include('footer.php');
?>
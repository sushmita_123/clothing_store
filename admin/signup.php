<?php


include('connection.php');

if(isset($_POST['submit'])){
	$username = mysqli_real_escape_string($conn,$_POST['uname']);
	$password = mysqli_real_escape_string($conn,$_POST['rpass']);
	$cpassword = mysqli_real_escape_string($conn,$_POST['rcpass']);

	
	$query = mysqli_query($conn,"select * from admin where username='$username'");
	$usecount=mysqli_num_rows($query);

	if($usecount>0){
		echo "<script type='text/javascript'>alert(' Username Exists');
		window.location='signup.php';
</script>";
			
	}else{
	if($password=== $cpassword){
		$epass=md5($password);
		$insertquery = "insert into admin(username,password) values('$username','$epass')";
		$query1=mysqli_query($conn,$insertquery);
		if($query1){
			?>
			<script type="text/javascript"> 
			alert("Registered!!"); location="login.php";
		</script>
				
			<?php
			// header('location:login.php');
		}else{
			?>
			<script type="text/javascript"> 
			alert("Not Registered"); 
				location="signup.php";
		</script>
			
			<?php
			// header('location:register.php');
		}
		
}
	else{
		?>
			<script type="text/javascript"> 
			alert("Password Not Matched");
			location="signup.php";
			 </script>
			
			<?php
	//header('location:register.php');
	}
}
}
?>
<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				
				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl ">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
					</div>
					<div class="panel-body">
						<form action="" method="POST">
							<div class="form-group mb-lg">
								<label>Name</label>
								<input name="uname" type="text" class="form-control input-lg" />
							</div>

							<div class="form-group mb-none">
								<div class="row">
									<div class="col-sm-6 mb-lg">
										<label>Password</label>
										<input name="rpass" type="password" class="form-control input-lg" />
									</div>
									<div class="col-sm-6 mb-lg">
										<label>Password Confirmation</label>
										<input name="rcpass" type="password" class="form-control input-lg" />
									</div>
								</div>
							</div>

							<div class="row">
								
								 <div class="form-group mt-4 mb-0">
                                                <input name="submit" value="Create Account" id="submit" type="submit" class="btn btn-primary btn-block"></div>
							</div>

							<span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

							

							<p class="text-center">Already have an account? <a href="login.php">Sign In!</a>

						</form>
					</div>
				</div>

				
			</div>
		</section>
		<!-- end: page -->
<?php 
include('footer.php');
?>
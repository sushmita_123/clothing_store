<?php
include('header.php');
include('connection.php');



?>
<div class="container-fluid" style="overflow:scroll;">
<section role="main" class="content-body" >
					<header class="page-header">
						<h2>Dashboard</h2>
					
						<div class="right-wrapper ">
							<ol class="breadcrumbs">
								<li>
									<a href="index.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Dashboard</span></li>
							</ol></div><br><br>
								
							 <table class="table table-dark">
							 	<h2 align="left" style="color: black">Categories </h2>
                        <thead >
                            <tr>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Operation</th>
                            </tr>
                            
                        </thead>
                        <tbody class="table table-light">
                            <?php
                           
                           // $db = mysqli_select_db($conn,'grands');
                            $sql = "select * from categories";
                            $query = mysqli_query($conn,$sql);
                            if (mysqli_num_rows($query) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($query)) {  
                                ?>

                            <tr>
                                
                                <td><?php echo $row['cat_name'] ?></td>
                                
                                <td><?php echo "<img src='".$row['cat_image']."' height='50px' width='auto'>" ?></td>
                                <td><a type="submit" href="editcategories.php?cat_id=<?php echo $row['cat_id']; ?>">Edit<i  class="fa fa-edit mt-3 ml-3" ></i></a> || <a href="deletecategories.php?cat_id=<?php echo $row['cat_id']; ?> "><i class="fa fa-trash mt-3 ml-3" id="btn-confirm" >Delete</i></a></td> 
                                </tr>
                           


<?php }}?>
 </tbody>
 </table>

							<div class="container" >
								
							 <table class="table table-dark">
							 	<h2 align="center" style="color: black"> Products</h2>
                        <thead >
                            <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Categories</th>
                            <th>Image</th>
                            <th>Operation</th>
                            </tr>
                        </thead>
                        <tbody class="table table-light">
                            <?php
                           
                           // $db = mysqli_select_db($conn,'grands');
                            $sql = "select * from products";
                            $query = mysqli_query($conn,$sql);
                            if (mysqli_num_rows($query) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($query)) {  
                                ?>

                            <tr>
                                
                                <td><?php echo $row['product_name'] ?></td>
                                <td><?php echo $row['product_price']?></td>
                                <td><?php echo $row['product_category']?></td>
                                <td><?php echo "<img src='".$row['product_image']."' height='50px' width='auto'>" ?></td>
                                <td><a type="submit" href="editproduct.php?product_id=<?php echo $row['product_id']; ?>">Edit<i  class="fa fa-edit mt-3 ml-3" ></i></a> || <a href="deleteproduct.php?product_id=<?php echo $row['product_id']; ?>">Delete<i class="fa fa-trash mt-3 ml-3" id="btn-confirm" ></i></a></td>
                            </tr>
                   
                         

<?php }}?>
					
     </tbody>
</table>		
<style type="text/css">
	.table-cat{
		 padding: 15px;
	}
	table{
		 border-bottom: solid 1px black;
	}
	.table-product{
		padding: 15px;
	}
</style>
</div>
</section>
</div>
</body>
<?php 
include('footer.php');
?>

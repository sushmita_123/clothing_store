<?php include('header.php');?>
		<!--banner-->
		<div class="banner-w3">
			<div class="demo-1">            
				<div id="example1" class="core-slider core-slider__carousel example_1">
					<div class="core-slider_viewport">
						<div class="core-slider_list">
							<div class="core-slider_item">
								<img src="images/a (2).jpg" class="img-responsive" alt="">
							</div>
							 <div class="core-slider_item">
								 <img src="images/b5.jpg" class="img-responsive" alt="">
							 </div>
							<div class="core-slider_item">
								  <img src="images/b3.jpg" class="img-responsive" alt="">
							</div>
							<div class="core-slider_item">
								  <img src="images/b1.jpg" class="img-responsive" alt="">
							</div>
						 </div>
					</div>
					<div class="core-slider_nav">
						<div class="core-slider_arrow core-slider_arrow__right"></div>
						<div class="core-slider_arrow core-slider_arrow__left"></div>
					</div>
					<div class="core-slider_control-nav"></div>
				</div>
			</div>
			<link href="css/coreSlider.css" rel="stylesheet" type="text/css">
			<script src="js/coreSlider.js"></script>
			<script>
			$('#example1').coreSlider({
			  pauseOnHover: false,
			  interval: 3000,
			  controlNavEnabled: true
			});

			</script>

		</div>	
		<!--banner-->
			<!--content-->
		<div class="content">
			
			<!--new-arrivals-->
				<div class="new-arrivals-w3agile">
					<div class="container">
						<h2 class="tittle">New Arrivals</h2>
						<div class="arrivals-grids">
							<div class="col-md-3 arrival-grid simpleCart_shelfItem">
								<div class="grid-arr">
									<div  class="grid-arrival">
										<figure>		
											<a href="#" class="new-grid" data-toggle="modal" data-target="#myModal1">
												<div class="grid-img">
													<img  src="images/p6.jpg" class="img-responsive" alt="">
												</div>
												<div class="grid-img">
													<img  src="images/p5.jpg" class="img-responsive"  alt="">
												</div>			
											</a>		
										</figure>	
									</div>
									<div class="ribben">
										<p>NEW</p>
									</div>
									
									<div class="block">
										<div class="starbox small ghosting"> </div>
									</div>
									<div class="women">
										
										<span class="size">XL / XXL / S </span>
										<p ><em class="item_price">1700.00</em></p>
										<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
									</div>
								</div>
							</div>
							<div class="col-md-3 arrival-grid simpleCart_shelfItem">
								<div class="grid-arr">
									<div  class="grid-arrival">
										<figure>		
											<a href="#" class="new-gri" data-toggle="modal" data-target="#myModal2">
												<div class="grid-img">
													<img  src="images/p7.jpg" class="img-responsive" alt="">
												</div>
												<div class="grid-img">
													<img  src="images/p8.jpg" class="img-responsive"  alt="">
												</div>			
											</a>		
										</figure>	
									</div>
									<div class="ribben2">
										<p>New</p>
									</div>
									<div class="block">
										<div class="starbox small ghosting"> </div>
									</div>
									<div class="women">
										
										<span class="size">XL / XXL / S </span>
										<p ><em class="item_price">1200.00</em></p>
										<a href="#" data-text="Add To Cart" class=" my-cart-b item_add">Add To Cart</a>
									</div>
								</div>
							</div>
							<div class="col-md-3 arrival-grid simpleCart_shelfItem">
								<div class="grid-arr">
									<div  class="grid-arrival">
										<figure>		
											<a href="#" class="new-gri" data-toggle="modal" data-target="#myModal3">
												<div class="grid-img">
													<img  src="images/p10.jpg" class="img-responsive" alt="">
												</div>
												<div class="grid-img">
													<img  src="images/p9.jpg" class="img-responsive"  alt="">
												</div>			
											</a>		
										</figure>	
									</div>
									<div class="ribben1">
										<p>New</p>
									</div>
									<div class="block">
										<div class="starbox small ghosting"> </div>
									</div>
									<div class="women">
										
										<span class="size">XL / XXL / S </span>
										<p ><em class="item_price">1500.00</em></p>
										<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
									</div>
								</div>
							</div>
							<div class="col-md-3 arrival-grid simpleCart_shelfItem">
								<div class="grid-arr">
									<div  class="grid-arrival">
										<figure>		
											<a href="#" class="new-gri" data-toggle="modal" data-target="#myModal4">
												<div class="grid-img">
													<img  src="images/p11.jpg" class="img-responsive" alt="">
												</div>
												<div class="grid-img">
													<img  src="images/p12.jpg" class="img-responsive"  alt="">
												</div>			
											</a>		
										</figure>	
									</div>
									<div class="ribben1">
										<p>New</p>
									</div>
									<div class="block">
										<div class="starbox small ghosting"> </div>
									</div>
									<div class="women">
										
										<span class="size">XL / XXL / S </span>
										<p ><em class="item_price">2500.00</em></p>
										<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			<!--new-arrivals-->
				<!--accessories-->
			
			<!--accessories-->
			<!--Products-->
				<div class="product-agile">
					<div class="container">
						<h3 class="tittle1"> New Products</h3>
						<div class="slider">
							<div class="callbacks_container">
								<ul class="rslides" id="slider">
									<li>	 
										<div class="caption">
											<div class="col-md-3 cap-left simpleCart_shelfItem">
												<div class="grid-arr">
													<div  class="grid-arrival">
														<figure>		
															<a href="single.html">
																<div class="grid-img">
																	<img  src="images/p14.jpg" class="img-responsive" alt="">
																</div>
																<div class="grid-img">
																	<img  src="images/p13.jpg" class="img-responsive"  alt="">
																</div>			
															</a>		
														</figure>	
													</div>
													<div class="block">
														<div class="starbox small ghosting"> </div>
													</div>
													<div class="women">
														
														<span class="size">XL / XXL / S </span>
														<p ><em class="item_price">7500</em></p>
														<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
													</div>
												</div>
											</div>
											<div class="col-md-3 cap-left simpleCart_shelfItem">
												<div class="grid-arr">
													<div  class="grid-arrival">
														<figure>		
															<a href="single.html">
																<div class="grid-img">
																	<img  src="images/p15.jpg" class="img-responsive" alt="">
																</div>
																<div class="grid-img">
																	<img  src="images/p16.jpg" class="img-responsive"  alt="">
																</div>			
															</a>		
														</figure>	
													</div>
													
													<div class="block">
														<div class="starbox small ghosting"> </div>
													</div>
													<div class="women">
														
														<span class="size">XL / XXL / S </span>
														<p ><em class="item_price">1000</em></p>
														<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
													</div>
												</div>
											</div>
											<div class="col-md-3 cap-left simpleCart_shelfItem">
												<div class="grid-arr">
													<div  class="grid-arrival">
														<figure>		
															<a href="single.html">
																<div class="grid-img">
																	<img  src="images/p18.jpg" class="img-responsive" alt="">
																</div>
																<div class="grid-img">
																	<img  src="images/p17.jpg" class="img-responsive"  alt="">
																</div>			
															</a>		
														</figure>	
													</div>
													
													<div class="block">
														<div class="starbox small ghosting"> </div>
													</div>
													<div class="women">
														
														<span class="size">XL / XXL / S </span>
														<p ><em class="item_price">1600</em></p>
														<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
													</div>
												</div>
											</div>
											<div class="col-md-3 cap-left simpleCart_shelfItem">
												<div class="grid-arr">
													<div  class="grid-arrival">
														<figure>		
															<a href="single.html">
																<div class="grid-img">
																	<img  src="images/p20.jpg" class="img-responsive" alt="">
																</div>
																<div class="grid-img">
																	<img  src="images/p19.jpg" class="img-responsive"  alt="">
																</div>			
															</a>		
														</figure>	
													</div>
													
													<div class="block">
														<div class="starbox small ghosting"> </div>
													</div>
													<div class="women">
														
														<span class="size">XL / XXL / S </span>
														<p ><em class="item_price">999</em></p>
														<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</li>
									<li>	 
										<div class="caption">
											<div class="col-md-3 cap-left simpleCart_shelfItem">
												<div class="grid-arr">
													<div  class="grid-arrival">
														<figure>		
															<a href="single.html">
																<div class="grid-img">
																	<img  src="images/p21.jpg" class="img-responsive" alt="">
																</div>
																<div class="grid-img">
																	<img  src="images/p22.jpg" class="img-responsive"  alt="">
																</div>			
															</a>		
														</figure>	
													</div>
													<div class="block">
														<div class="starbox small ghosting"> </div>
													</div>
													<div class="women">
														
														<span class="size">XL / XXL / S </span>
														<p ><em class="item_price">1499</em></p>
														<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
													</div>
												</div>
											</div>
											<div class="col-md-3 cap-left simpleCart_shelfItem">
												<div class="grid-arr">
													<div  class="grid-arrival">
														<figure>		
															<a href="single.html">
																<div class="grid-img">
																	<img  src="images/p24.jpg" class="img-responsive" alt="">
																</div>
																<div class="grid-img">
																	<img  src="images/p23.jpg" class="img-responsive"  alt="">
																</div>			
															</a>		
														</figure>	
													</div>
													
													<div class="block">
														<div class="starbox small ghosting"> </div>
													</div>
													<div class="women">
														
														<span class="size">XL / XXL / S </span>
														<p ><em class="item_price">1600</em></p>
														<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
													</div>
												</div>
											</div>
											<div class="col-md-3 cap-left simpleCart_shelfItem">
												<div class="grid-arr">
													<div  class="grid-arrival">
														<figure>		
															<a href="single.html">
																<div class="grid-img">
																	<img  src="images/p26.jpg" class="img-responsive" alt="">
																</div>
																<div class="grid-img">
																	<img  src="images/p25.jpg" class="img-responsive"  alt="">
																</div>			
															</a>		
														</figure>	
													</div>
													
													<div class="block">
														<div class="starbox small ghosting"> </div>
													</div>
													<div class="women">
														
														<span class="size">XL / XXL / S </span>
														<p ><em class="item_price">1900</em></p>
														<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
													</div>
												</div>
											</div>
											<div class="col-md-3 cap-left simpleCart_shelfItem">
												<div class="grid-arr">
													<div  class="grid-arrival">
														<figure>		
															<a href="single.html">
																<div class="grid-img">
																	<img  src="images/p10.jpg" class="img-responsive" alt="">
																</div>
																<div class="grid-img">
																	<img  src="images/p9.jpg" class="img-responsive"  alt="">
																</div>			
															</a>		
														</figure>	
													</div>
													
													<div class="block">
														<div class="starbox small ghosting"> </div>
													</div>
													<div class="women">
														
														<span class="size">XL / XXL / S </span>
														<p ><em class="item_price">999</em></p>
														<a href="#" data-text="Add To Cart" class="my-cart-b item_add">Add To Cart</a>
													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="accessories-w3l">
				<div class="container">
					<h3 class="tittle">20% Discount on</h3>
					<span>TRENDING DESIGNS</span>
					
				</div>
			</div>
			<!--Products-->
			<div class="latest-w3">
				<div class="container">
					<h3 class="tittle1">Latest Fashion Trends</h3>
					<div class="latest-grids">
						<div class="col-md-4 latest-grid">
							<div class="latest-top">
								<img  src="images/l1.jpg" class="img-responsive"  alt="">
								<div class="latest-text">
									<h4>Men</h4>
								</div>
								<div class="latest-text2 hvr-sweep-to-top">
									<h4>-50%</h4>
								</div>
							</div>
						</div>
						
						<div class="col-md-4 latest-grid">
							<div class="latest-top">
								<img  src="images/l3.jpg" class="img-responsive"  alt="">
								<div class="latest-text">
									<h4>Women</h4>
								</div>
								<div class="latest-text2 hvr-sweep-to-top">
									<h4>-50%</h4>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="latest-grids">
						<div class="col-md-4 latest-grid">
							<div class="latest-top">
								<img  src="images/21.jpg" class="img-responsive"  alt="">
								<div class="latest-text">
									<h4>kids</h4>
								</div>
								<div class="latest-text2 hvr-sweep-to-top">
									<h4>-50%</h4>
								</div>
							</div>
						</div>
						</div>
						
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
				
					</div>
				</div>
			<!--new-arrivals-->
		</div>
		<!--content-->
<?php include('footer.php');?>
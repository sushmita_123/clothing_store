<?php include('header.php');


include('admin/connection.php');

if(isset($_POST['submit'])){
	$username = mysqli_real_escape_string($conn,$_POST['customer_name']);
	$password = mysqli_real_escape_string($conn,$_POST['customer_password']);
	$cpassword = mysqli_real_escape_string($conn,$_POST['rcpass']);
	$contact_information=mysqli_real_escape_string($conn,$_POST['contact_info']);

	
	$query = mysqli_query($conn,"select * from customer where customer_name='$username'");
	$usecount=mysqli_num_rows($query);

	if($usecount>0){
		echo "<script type='text/javascript'>alert(' Username Exists');
		window.location='signup.php';
</script>";
			
	}else{
	if($password=== $cpassword){
		$epassword=md5($password);
		$insertquery = "insert into customer(customer_name,customer_password,contact_info) values('$username','$epassword','$contact_information')";
		$query1=mysqli_query($conn,$insertquery);
		if($query1){
			?>
			<script type="text/javascript"> 
			alert("Registered!!");  location="login.php";
		</script>
				
			<?php
			 
		}else{
			?>
			<script type="text/javascript"> 
			alert("Not Registered"); 
				location="signup.php";
		</script>
			
			<?php
			
		}
		
}
	else{
		?>
			<script type="text/javascript"> 
			alert("Password Not Matched");
			location="signup.php";
			 </script>
			
			<?php
	
	}
}
}
?>

	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				
				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl ">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
					</div>
					<div class="panel-body">
						<form action="" method="POST">
							<div class="form-group mb-lg">
								<label>Name</label>
								<input name="customer_name" type="text" class="form-control input-lg" />
							</div>

							<div class="form-group mb-none">
								<div class="row">
									<div class="col-sm-6 mb-lg">
										<label>Password</label>
										<input name="customer_password" type="password" class="form-control input-lg" />
									</div>
									<div class="col-sm-6 mb-lg">
										<label>Password Confirmation</label>
										<input name="rcpass" type="password" class="form-control input-lg" />
									</div>
								</div>
							</div>
							<div class="form-group mb-lg">
								<label>Contact info</label>
								<input name="contact_info" type="number" class="form-control input-lg" />
							</div>

							<div class="row">
								
								 <div class="form-group mt-4 mb-0">
                                                <input name="submit" value="Create Account" id="submit" type="submit" class="btn btn-primary btn-block"></div>
							</div>

							<span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

							

							<p class="text-center">Already have an account? <a href="login.php">Sign In!</a>

						</form>
					</div>
				</div>

				
			</div>
		</section>
		<!-- end: page -->
<?php 
include('footer.php');
?>